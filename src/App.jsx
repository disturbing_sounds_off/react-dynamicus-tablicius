import { useState } from 'react'
import './App.css'

function App() {

    const fullscreen = {
        width: "100vh",
        height: "100vh",
    };

    const mainFormStyle = {
        display: "flex",
        flexDirection: "row",
        padding: "4px",
        backgroundColor: "lightBlue",
        borderRadius: "14px",
        marginBottom: "14px",
    }

    const formStyle = {
        display: "flex",
        flexDirection: "row",
        padding: "4px",
        backgroundColor: "pink",
        borderRadius: "14px",
        marginBottom: "14px",
        height: "45px"
    }

    const inputStyle = {
        flex: "1",
        margin: "2px",
        paddingLeft: "12px",
        borderRadius: "14px",
        backgroundColor: "white",
        border: "none",
        color: "black",
        fontSize: "18px"
    }

    const labelDivStyle = {
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        margin: "2px",
        width: "55px",
        paddingLeft: "12px",
        paddingRight: "12px",
        borderRadius: "14px",
        backgroundColor: "white",
        border: "none",
        color: "black",
        fontSize: "18px"
    }


    const defaultValues = [
        {
            id: 1,
            name: "Имя",
            value: "test",
        },
        {
            id: 2,
            name: "Расса",
            value: "test",
        },
        {
            id: 3,
            name: "Оружие",
            value: "test",
        },
    ];

    const [newFieldName, setNewFieldName] = useState("");
    const [list, setList] = useState(defaultValues);

    const handleAddField = (newName) => {
        let newId = list[list.length -1].id + 1
        setList((old) => [
            ...old,
            {
                id: newId,
                name: newName,
                value: "test"
            }
        ])
    };

    const handleChangeFieldValue = (id, newValue) => {
        setList(
            list.map((item) => 
                item.id === id
                ? {...item, value: newValue}
                : {...item}
            )
        )
    }

    return (
        <>
            <div style={fullscreen}>
                <div 
                style={mainFormStyle}>
                    <input
                        style={inputStyle}
                        value={newFieldName}
                        onChange={(e) => setNewFieldName(e.target.value)}
                        type="text" />


                    <button
                        style={{margin: "4px"}}
                        onClick={() => {
                            handleAddField(newFieldName);
                            setNewFieldName("");
                        }}>
                        add field
                    </button>
                </div>

                {list.map((item, index) => (
                    <div style={formStyle} key={index}>
                        <div style={labelDivStyle}>
                            {item.name}
                        </div>
                        <input 
                            type="text"
                            style={inputStyle}
                            value={item.value}
                            onChange={(e)=> handleChangeFieldValue(item.id, e.target.value)}
                        />
                    </div>
                ))}

                
                <h4>{
                     list.map(item => `${item.name}, ${item.value} | `)
                }</h4>

                <div style={{height: "30px"}}></div>
                <button
                    onClick={() => console.log(list)}
                >
                    print to console
                </button>
            </div>
        </>
    )
}

export default App
